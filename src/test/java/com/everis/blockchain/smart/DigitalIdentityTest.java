package com.everis.blockchain.smart;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.utils.Numeric;

import com.everis.blockchain.smart.http.EthCoreParams;
import com.everis.blockchain.smart.impl.DigitalIdentityImpl;

public class DigitalIdentityTest {

    EthCoreParams ethCoreParams = new EthCoreParams("https://35.197.76.152", "0x123ecd2cccbe2ca6d705d4133145ed3062e276d2a8724616de1f5791c2bbe8e", null, BigInteger.valueOf(0), BigInteger.valueOf(4000000));
    private DigitalIdentityImpl IM = new DigitalIdentityImpl("0xa6b4540a2bfbe8663caa78027c83d0dcb1b7c837", ethCoreParams);
    private String aduana = "0xe0ef4c43eeedefa560b2cad5923e3c6d85c4e075";

	private static final Logger log = LoggerFactory.getLogger(DigitalIdentityTest.class);
    
	
	
	 
    //@Test
	public void deployIM() throws IOException, Exception {
		String contractAddress = IM.deployIM();
		log.info("contractAddress: " + contractAddress);
		assertThat(contractAddress, containsString("0x"));
	}
    
    //@Test
	public void createIdentity() throws Exception {
        String keyMnemonic = "admin";
        String keyProfile = "public";
        String urlProfile = "Qmxxx";
        String username = "test";
        String salt = "salt";
		String proxy = IM.createIdentity(keyMnemonic, keyProfile, urlProfile, username, salt);
		log.info("New identity: " + proxy);
		assertThat(proxy, containsString("0x"));
    }

    //@Test
    public void setCap() throws Exception {
        String cap = "fw";
        Date date= new Date();
        long time = date.getTime() / 1000;
        String txHash = IM.setCap(aduana, "", cap, BigInteger.valueOf(time), BigInteger.valueOf(0));
        assertThat(txHash, containsString("0x"));
    }

   //@Test
    public void checkCap() throws Exception {
        String cap = "fw";
        Boolean hasCap = IM.checkCap(aduana, "", cap);
        assertTrue(hasCap);
    }
    
    //@Test
    public void forwardToTest() throws IOException, Exception {
        String destination = "0xd69399c5f03762315946dd44bAdcdeC2451D6633";
        Function function = new Function("setTest", // Function name
            Arrays.asList(new Utf8String("Test")), // Function input parameters
            Collections.emptyList());
        String encodedFunction = FunctionEncoder.encode(function);
        byte[] data = Numeric.hexStringToByteArray(Numeric.cleanHexPrefix(encodedFunction));
        String txHash = IM.forwardTo(aduana, destination, BigInteger.valueOf(0), data);
        assertThat(txHash, containsString("0x"));
    }

  

}