package com.everis.blockchain.smart.main.model;

import java.math.BigInteger;
import java.util.LinkedHashMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.everis.blockchain.smart.http.EthCoreParams;
import com.everis.blockchain.smart.impl.DigitalIdentityImpl;
import com.everis.blockchain.smart.util.Constants;

public class GenerateIdentityManager {

	private static final Logger log = LoggerFactory.getLogger("GenerateIdentityManager");

	private EthCoreParams ethCoreParams;
	private DigitalIdentityImpl DIM;
	// private String proxyAddressAduana ;
	private LinkedHashMap<String, String> map;
//	private RolesImpl roles;

	public GenerateIdentityManager(LinkedHashMap<String, String> map) {
		this.map = map;

		ethCoreParams = new EthCoreParams(map.get(Constants.BLOCKCHAIN_SERVER), map.get(Constants.PRIVATE_KEY_BACKEND),
				null, BigInteger.valueOf(0), BigInteger.valueOf(4000000));

		
	}

	public void create() throws Exception {
	
		if (StringUtils.isEmpty(map.get(Constants.CONTRACT_ADDRESS_IDENTITY_MANAGER))) {
			deployIdentityManager();
		}else {
			DIM = new DigitalIdentityImpl(map.get(Constants.CONTRACT_ADDRESS_IDENTITY_MANAGER), ethCoreParams);
		}
		
		if (StringUtils.isEmpty(map.get(Constants.CONTRACT_ADDRESS_PROXY_FIRST_ADMINISTRATOR))) {
			createIdentity();
		}

	

		

	}

	private void deployIdentityManager() throws Exception {
		DIM = new DigitalIdentityImpl(null, ethCoreParams);
		map.put(Constants.CONTRACT_ADDRESS_IDENTITY_MANAGER, DIM.addressIM);
		log.info("CONTRACT_ADDRESS_IDENTITY_MANAGER : " + map.get(Constants.CONTRACT_ADDRESS_IDENTITY_MANAGER));
	}

	private void createIdentity() throws Exception {

		String keyMnemonic = "";
		String keyProfile = "";
		String urlProfile = "";
		String username = "";
		String salt = "";

		String proxyAddress = DIM.createIdentity(keyMnemonic, keyProfile, urlProfile, username, salt);

		map.put(Constants.CONTRACT_ADDRESS_PROXY_FIRST_ADMINISTRATOR, proxyAddress);

		log.info("CONTRACT_ADDRESS_PROXY_FIRST_ADMINISTRATOR : "
				+ map.get(Constants.CONTRACT_ADDRESS_PROXY_FIRST_ADMINISTRATOR));

	}


}
