package com.everis.blockchain.smart.main;

import java.util.LinkedHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.everis.blockchain.smart.main.model.GenerateIdentityManager;
import com.everis.blockchain.smart.main.model.GeneratePrivateKey;
import com.everis.blockchain.smart.util.UtilYaml;

public class InitLoad {
	private static final Logger log = LoggerFactory.getLogger("InitLoad");

	
	public static void main(String[] args) {
		try {
			
			// mvn clean assembly:assembly -DdescriptorId=jar-with-dependencies
			// java -cp blockchain-honduras-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.everis.blockchain.smart.main.InitLoad
			log.info("---INIT--");
			 
			LinkedHashMap<String, String> map = UtilYaml.readFile();

			new GeneratePrivateKey(map).create();
			new GenerateIdentityManager(map).create();
			
			UtilYaml.saveFile(map);
		
			UtilYaml.saveFileSpring(map); 
			
		
			log.info("---FINISH OK--");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
