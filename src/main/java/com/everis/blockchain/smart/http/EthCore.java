package com.everis.blockchain.smart.http;

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Keys;
import org.web3j.crypto.Wallet;
import org.web3j.crypto.WalletFile;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.besu.Besu;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGasPrice;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.BesuPrivateTransactionManager;
import org.web3j.tx.gas.BesuPrivacyGasProvider;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.StaticGasProvider;

import com.everis.blockchain.smart.util.SslUtils;

import io.reactivex.annotations.Nullable;
import okhttp3.OkHttpClient;

public class EthCore {

	private static final Logger log = LoggerFactory.getLogger(EthCore.class);
	private Web3j web3;
	private Besu besu;
	private Credentials credentials;
	private String networkId;
	private BesuPrivateTransactionManager transactionManager;
	public BigInteger gasPrice;
	public BigInteger gasLimit;

	public EthCore(EthCoreParams params) {
		web3 = Web3j.build(new HttpService(params.host, createOkHttpClient(params.host.contains("https:")), true));
		initVars(params);
	}

	public EthCore(EthCoreParams params, String privGroupId, String from) {
		besu = Besu.build(new HttpService(params.host, createOkHttpClient(params.host.contains("https:")), true));
		web3 = Web3j.build(new HttpService(params.host, createOkHttpClient(params.host.contains("https:")), true));
		initVars(params);
	}

	private void initVars(EthCoreParams params) {
		try {
			String privateKey = generatePrivateKey(params);
			credentials = Credentials.create(privateKey);
			networkId = web3.netVersion().send().getResult();
			gasPrice = params.gasPrice != null ? params.gasPrice : getGasPrice();
			gasLimit = params.gasLimit != null ? params.gasLimit : getGasLimit();
		} catch (Exception e) {
			log.error("Error.initVars", e);
			log.error(e.getMessage());
		}
	}

	private String generatePrivateKey(EthCoreParams params) {
		String privateKey = null;
		try {
			privateKey = params.privateKey != null ? params.privateKey : createCredentials(params.seed).privateKey;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return privateKey;
	}

	public Web3j getWeb3jInstance() {
		return web3;
	}

	Besu getBesuInstance() {
		return besu;
	}

	BesuPrivateTransactionManager getTransactionManagerBesu() {
		return transactionManager;
	}

	public ContractGasProvider getDefaultContractGasProvider() throws IOException {
		ContractGasProvider contractGasProvider = new StaticGasProvider(gasPrice, gasLimit);
		return contractGasProvider;
	}

	BesuPrivacyGasProvider getDefaultBesuPrivacyGasProvider() {
		BesuPrivacyGasProvider besuPrivacyGasProvider = new BesuPrivacyGasProvider(gasPrice, gasLimit);
		return besuPrivacyGasProvider;
	}

	public Account createCredentials(String seed) throws CipherException, InvalidAlgorithmParameterException,
			NoSuchAlgorithmException, NoSuchProviderException {
		ECKeyPair ecKeyPair = Keys.createEcKeyPair();
		BigInteger privateKeyInDec = ecKeyPair.getPrivateKey();
		BigInteger publicKeyInDec = ecKeyPair.getPublicKey();
		String privatekeyInHex = privateKeyInDec.toString(16);
		String publickeyInHex = publicKeyInDec.toString(16);
		WalletFile wallet = Wallet.createLight(seed, ecKeyPair);
		String address = wallet.getAddress();
		Account account = new Account("0x" + address, "0x" + privatekeyInHex, publickeyInHex);
		return account;
	}

	public void updatePrivateKey(String privateKey) {
		credentials = Credentials.create(privateKey);
	}

	public Credentials getCredentials() {
		return credentials;
	}

	public String getNetworkId() {
		return networkId;
	}

	public BigInteger getGasPrice() throws IOException {
		EthGasPrice ethGasPrice = web3.ethGasPrice().send();
		BigInteger gasPrice = ethGasPrice.getGasPrice();
		return gasPrice;
	}

	public BigInteger getGasLimit() throws IOException {
		BigInteger blockGasLimit = web3.ethGetBlockByNumber(DefaultBlockParameterName.LATEST, false).send().getBlock()
				.getGasLimit();
		return blockGasLimit;
	}

	public BigInteger getNonce(@Nullable String address) throws InterruptedException, ExecutionException {
		String addressAccount = address != null ? address : credentials.getAddress();
		EthGetTransactionCount ethGetTransactionCount = web3
				.ethGetTransactionCount(addressAccount, DefaultBlockParameterName.LATEST).sendAsync().get();
		BigInteger nonce = ethGetTransactionCount.getTransactionCount();
		return nonce;
	}




	private OkHttpClient createOkHttpClient(boolean isSSL) {
		OkHttpClient.Builder builder = isSSL ? SslUtils.trustAllSslClient() : new OkHttpClient.Builder();

		// builder.retryOnConnectionFailure(true);
		// configureLogging(builder);
		configureTimeouts(builder);
		return builder.build();
	}

	private void configureTimeouts(OkHttpClient.Builder builder) {
		int tos = 30;

		builder.connectTimeout(tos, TimeUnit.SECONDS);
		builder.readTimeout(tos, TimeUnit.SECONDS); // Sets the socket timeout too
		builder.writeTimeout(tos, TimeUnit.SECONDS);

	}

//	private static void configureLogging(OkHttpClient.Builder builder) {
//		if (log.isDebugEnabled()) {
//			HttpLoggingInterceptor logging = new HttpLoggingInterceptor(log::debug);
//			logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//			builder.addInterceptor(logging);
//		}
//	}

	void closeConnection() {
		web3.shutdown();
		besu.shutdown();
	}

}
