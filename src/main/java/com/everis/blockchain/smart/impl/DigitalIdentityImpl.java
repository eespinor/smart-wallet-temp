package com.everis.blockchain.smart.impl;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;

import com.everis.blockchain.smart.IDigitalIdentity;
import com.everis.blockchain.smart.contract.IdentityManager;
import com.everis.blockchain.smart.http.EthCore;
import com.everis.blockchain.smart.http.EthCoreParams;
import com.everis.blockchain.smart.util.Utils;

import io.reactivex.annotations.Nullable;

public class DigitalIdentityImpl implements IDigitalIdentity {

	private EthCore ethCore;
	public String addressIM;
	private IdentityManager imContract;
	private static final Logger log = LoggerFactory.getLogger(DigitalIdentityImpl.class);

	public DigitalIdentityImpl(@Nullable String identityManager, EthCoreParams ethCoreParams) {
		ethCore = new EthCore(ethCoreParams);
		try {
			addressIM = identityManager == null ? deployIM() : identityManager;
		} catch (Exception e) {
			addressIM = "0x0000000000000000000000000000000000000000";
			log.error("Error.DigitalIdentityImpl", e);
		}
		updateIMContract();
	}

	// Deploy Contract
	public String deployIM() throws IOException, Exception {
		IdentityManager contract = IdentityManager
				.deploy(ethCore.getWeb3jInstance(), ethCore.getCredentials(), ethCore.getDefaultContractGasProvider())
				.send();
		addressIM = contract.getContractAddress();
		updateIMContract();
		return addressIM;
	}

	public String createIdentity(String keyMnemonic, String keyProfile, String urlProfile, String username, String salt)
			throws Exception {
		TransactionReceipt transactionReceipt = imContract.createIdentity(Utils.stringToBytes(keyMnemonic, 16),
				Utils.stringToBytes(keyProfile, 16), urlProfile, username, salt).send();
		List<Log> logs = transactionReceipt.getLogs();
		String proxy = null;
		for (Log logTx : logs) {
			// IdentityCreated
			if (logTx.getTopics().get(0).equals("0xac993fde3b9423ff59e4a23cded8e89074c9c8740920d1d870f586ba7c5c8cf0")) {
				proxy = "0x" + logTx.getData().substring(logTx.getData().length() - 40);
			}
		}
		if (proxy != null)
			return proxy;
		throw new Error("Could not create identity");
	}

	public String setCap(String identity, String device, String cap, BigInteger startDate, BigInteger endDate)
			throws Exception {
		TransactionReceipt transactionReceipt = imContract.setCap(identity, device, cap, startDate, endDate).send();
		return transactionReceipt.getTransactionHash();
	}

	public Boolean checkCap(String identity, String device, String cap) throws Exception {
		Boolean hasCap = imContract.hasCap(identity, device, cap).send();
		return hasCap;
	}

	public String forwardTo(String identity, String destination, BigInteger value, byte[] data) throws Exception {
		TransactionReceipt transactionReceipt = imContract.forwardTo(identity, destination, value, data).send();
		if (transactionReceipt.getStatus().equals("0x1"))
			return transactionReceipt.getTransactionHash();
		throw new Error("Could not forward transaction");
	}

	private void updateIMContract() {
		try {
			imContract = IdentityManager.load(addressIM, ethCore.getWeb3jInstance(), ethCore.getCredentials(),
					ethCore.getDefaultContractGasProvider());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}