package com.everis.blockchain.smart.util;

import org.web3j.protocol.exceptions.TransactionException;
import org.web3j.utils.Strings;

public class Utils {

	public static byte[] stringToBytes(String string, int lenght) {
		byte[] byteValue = string.getBytes();
		byte[] byteValueLen = new byte[lenght];
		System.arraycopy(byteValue, 0, byteValueLen, 0, byteValue.length);
		return byteValueLen;
	}

	public static String getRevertReason(String revertReason) {
		String errorMethodId = "0x08c379a0"; // Numeric.toHexString(Hash.sha3("Error(string)".getBytes())).substring(0,
												// 10)
		if (revertReason.startsWith(errorMethodId)) {
			return hexToAscii(revertReason.substring(274)).trim();
		}
		return "Transaction failed";
	}

	public static String getRevertReason(Exception e) {
		if (e instanceof TransactionException) {
			TransactionException tx = (TransactionException)e;
			if(tx.getTransactionReceipt() != null && tx.getTransactionReceipt().isPresent()) {
				return Utils.getRevertReason(tx.getTransactionReceipt().get().getRevertReason());
			}
			
		}
		return e != null ? e.getMessage() : "";
	}


	public static String hexToAscii(String hexStr) {
		StringBuilder output = new StringBuilder("");
		for (int i = 0; i < hexStr.length(); i += 2) {
			String str = hexStr.substring(i, i + 2);
			output.append((char) Integer.parseInt(str, 16));
		}
		return output.toString();
	}

	
	public static String getHexBlank(String value) {
		return Strings.isEmpty(value) ? "0x0000000000000000000000000000000000000000000000000000000000000000" : value;
	}
}
