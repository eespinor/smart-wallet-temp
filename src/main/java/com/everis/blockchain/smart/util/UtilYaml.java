package com.everis.blockchain.smart.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UtilYaml {
 	private static final Logger log = LoggerFactory.getLogger("Yaml");
	private static final String NAME_FILE = System.getProperty("user.dir") + "/resource/configuration.yaml";
	private static final String NAME_FILE_SPRING = System.getProperty("user.dir")
			+ "/resource/configurationSpring.yaml";

	public static void saveFile(LinkedHashMap<String, String> map) {

		log.info("<---SAVE---->");

		Path path = Paths.get(NAME_FILE);
		try (BufferedWriter writer = java.nio.file.Files.newBufferedWriter(path)) {

			for (Map.Entry<String, String> entry : map.entrySet()) {
				// System.out.println(" - " + entry.getKey() + " = " + entry.getValue());

				writer.write("  - " + entry.getKey() + "=" + entry.getValue() + "\n");
			}
		} catch (IOException io) {
			io.printStackTrace();
		}

	}

	public static void saveFileSpring(LinkedHashMap<String, String> map) {

		log.info("<---SAVE---->");

		Path path = Paths.get(NAME_FILE_SPRING);
		try (BufferedWriter writer = java.nio.file.Files.newBufferedWriter(path)) {

			for (Map.Entry<String, String> entry : map.entrySet()) {
				writer.write("  " +capitalize(entry.getKey()) + ": '" + entry.getValue() + "'\n");
			}
		} catch (IOException io) {
			io.printStackTrace();
		}

	}
	


	@SuppressWarnings("deprecation")
	private static String capitalize(String value) {
		return org.apache.commons.lang3.text.WordUtils.uncapitalize(org.apache.commons.lang3.text.WordUtils
				.capitalize(value.replaceAll("_", " ").toLowerCase()).replace(" ", ""));
	}

	public static LinkedHashMap<String, String> readFile() {
		try {
			// System.out.println("<---READ---->");
			LinkedHashMap<String, String> map = new LinkedHashMap<>();
			Scanner sc = new Scanner(new File(NAME_FILE));
			while (sc.hasNext()) {

				String str = sc.nextLine();
				String[] tokensVal = str.split("=");

				String key = tokensVal[0].replace('-', ' ').trim();
				String value = "";

				if (tokensVal.length >= 2) {
					value = tokensVal[1].trim();

				}

				if (!StringUtils.isEmpty(key)) {
					// System.out.println(key + " = " + value);
					map.put(key, value);
				}
			}
			sc.close();
			return map;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}
}
